package com.ehvlinc.servicediscoverer.Web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaServer
@ComponentScan("com.ehvlinc.servicediscoverer")
public class ServiceDiscovererApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDiscovererApplication.class, args);
	}

}

