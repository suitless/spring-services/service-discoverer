FROM openjdk:8-stretch
COPY /build/libs/com.ehvlinc.eureka-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8761
CMD "java" "-jar" "run.jar"
